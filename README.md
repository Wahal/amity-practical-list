# Amity Practicals 2K14 #

This Repository Contains All The Questions Form The Practicals Lists. 

### Mrinal Wahal ###

**Questions Are As Followed:****

1)	WAP to calculate the area and circumference of a circle if radius is given.

2)	WAP to find the simple interest if principal, rate and time are given

3)	Write a program to find the sum of first three multiples of a given number n

4)	Write a program to read your name, age and class and display message as - 
Welcome <name>
I am <age> years old and studying in class <class>
Last year I was <n> years old.

5)	WAP to find the denomination of currency in Re 1,2,5,10,50,100,500 and 1000 in a given amount entered by user.

6)	WAP to make a simple calculator that reads two numbers and an operator. On selection of an operator, perform operation and print the result as shown below-
		Enter first number: 5
		Enter second number: 7
		Enter operator: *
		Result: 5 * 7 = 35

7)	Read today's date as DD,MM,YYYY format and print it as DD- MMM - YY
Eg Input date-
Enter day 25
Enter month 4
Enter year 2014
Result:  25-APR -14

8)	You are driving a little too fast and the police officer stops you and issues a ticket. Write code to compute the result, encoded as an integer value: 0=no ticket, 1=small ticket, 2=big ticket. 
If speed is 60 or less, the result is 0. If speed is between 61 and 80 inclusive, the result is 1. If speed is 81 or more, the result is 2. Unless it is your birthday, on that day, your speed can be 5 higher in all cases.WAP to model above scenario.

9)	An electricity board charges according to the following rules:
For the first 100 units – 40 p per unit (p = Paise)
For the next 200 units - 50 p per unit
Beyond 300 units - 60 p per unit
All users have to pay meter charge also, which is Rs 50.
Write a program to read the number of units consumed and print out the charges.
Sample output
Input units: 150
Charges Rs. 115        [calculated as (100*.4 + 50*.50 +50)]

10)	WAP that reads n digit number. After reading the number, compute and display the sum of the odd positioned digits, multiply all even positioned digits and add these two numbers. 

11)	Write a program which will find all such numbers which are divisible by 7 but are not multiple of 5, between 2000 and 3200 (both included).

The numbers obtained should be printed in a comma-separated sequence on a single line.

12)	Develop a program to classify students amongst various categories as per their age entered. Read age of N students and count no of students falling in each category as described below  print a report as follows –
Group A: 12 yrs and above but less than 15 yrs	 - XX
Group B: 15 yrs and above but less than 17 yrs     - XX
Group C: 17 yrs and above but less than 19 yrs  	- XX 
Group D: Lesser than 12 yrs 			- XX

13)	Write a program to find if a number entered is a palindrome or not.

14)	A very famous numerical algorithm exists to find out the highest common factor (also called the greatest common divisor) of two numbers. The algorithm was invented by Euclid; it is therefore called the Euclid’s algorithm.
Here is how the algorithm works. Say you wish to find out the HCF of two numbers A and B. Simply repeat the following steps until both numbers become equal:

If A is greater than B, change A to A-B and do not modify B
If B is greater than A, change B to B-A and do not modify A

The value of A (and B) when both become equal will be the Highest Common 
Factor!

Let’s try this out with two numbers, A=18 and B=42.
B is greater than A, so B becomes 24 and A remains as 18. So, we have:
A = 18, B = 24 
If we repeat this process, we get:
A = 18, B = 6 A = 12, B = 6 A = 6, B = 6
Finally, both A and B become equal (both have value 6). The HCF of 18 and 42 is 6.

15)	Write a program to play the "Guess the number"-game, where the number to be guessed is randomly chosen between 1 and 20. This is how program should work when executed 
 (Only three chances allowed)
Hello! What is your name? 
Tanuj
Well, Tanuj, I have chosen of a number between 1 and 20. 
Take a guess. 
10 
Your guess is too low.
Take a guess.
 15 
Your guess is too low. 
Take a guess. 
18 
Good job, Tanuj! You guessed my number in 3 guesses!

16)	WAP to print the sum of n terms of  the  following series -
		a) x+ x2/2 + x3/3 + x4/4 +....+ xn/n
		b) 1/ (sqrt(2) + 2 / sqrt(3) + 3/ sqrt(4) +.......n/sqrt(n+1)

17)	A farm has chickens and rabbits. Farm owner counts number of heads and legs and wishes to find out number of chickens and rabbits? Write a program to solve this classic ancient Chinese puzzle.

If farm owner counts 35 heads and 94 legs. Calculate and print how many rabbits and chickens he has? 

18) WAP to print a Pascaline triangle of n lines. 

19)   Write a program to read a string and print 
i) Frequency of all characters
ii) Word of highest length
iii) The string in title case
	iv) Read full name as a string and print only the initials.
Eg- 
Enter name: Mohan Das Karam Chand Gandhi
Output string M D K C Gandhi.

20) Read two strings and check if string1 is prefix, postfix or nothing from the string2. 
          (Use RE expressions)
For Eg-
string1: ever
string2: evergreen
Output:  prefix

21) Data can be represented in memory in different ways Binary, Decimal, Octal, and Hexadecimal. Input number in decimal and desired type (Specify B for Binary O for Octal, H for Hexadecimal) for output.   

Write a program using UDF to perform the conversions-
SAMPLE INPUT      12 
DESIRED TYPE      B	
Result:  1100
SAMPLE INPUT        25
DESIRED TYPE        O	
Result: 	41

22)  In Cryptography, a Caesar cipher is a very simple encryption techniques in which each letter in the plain text is replaced by a letter some fixed number of positions down the alphabet list in a cyclic manner. For example, with a shift of 3, A would be replaced by D, B would become E, and so on. The method is named after Julius Caesar, who used it to communicate with his generals. ROT-13 ("rotate by 13 places"). 

	Create functions encrypt and decrypt which takes in a string and rotation (rotate by n places) and encrypts the string then decrypts the string. The program should be menu driven. Also if rotation is not specified then the encryption should take place by 13.

23)  Define a function histogram () that takes a list of numbers prints a histogram to the screen. 

      For example, histogram ([4, 9, 7]) should print the following:

****
*********
*******

Use above function to take a list of numbers, sort it and find frequencies of numbers present in the list and create a new list freq []. Pass this list as a parameter to histogram function to print the histogram.

24) Consider the following algorithm to generate a sequence of numbers and store them into a list. Start with an integer n. If n is even, divide by 2. If n is odd, multiply by 3 and add 1. Repeat this process with the new value of n, terminating when n = 1.

	 For example, the following sequence of numbers will be generated for n = 22:
			22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1
        
       Now do the following on above created list-
i) Print total number of elements in the list
ii) Print the sorted list
iii) Delete all occurrences of multiples of 10 and print it .

25) Read a string, convert it into list of words. Now apply following operations on the list (use sorted function)
i) Sort in reverse order and display
ii) Sort according to the first occurrence of capital alphabet present in the list
iii) Sort according to the length of words
iv) Create a function which takes a string as input and returns to its last character. Now apply sorting based on above function

26) Write functions max_even () and min_odd () returning to highest even number and lowest odd number respectively from the list passed as a parameter.

27) Write a program which takes 2 digits, X, Y as input and generates a 2-dimensional array. The element value in the i-th row and j-th column of the array should be i*j.
 Note: i=0,1.., X-1; j=0,1,¡¬Y-1.
Example
Suppose the following inputs are given to the program:
3,5
Then, the output of the program should be:
[[0, 0, 0, 0, 0], [0, 1, 2, 3, 4], [0, 2, 4, 6, 8]] 


28) Given below two dictionaries:

 relatives ={"Lisa" : "daughter", "Bart" : "son", "Marge" : "mother", "Homer" : "father", "Santa" : "dog"} 
ages = {“Lisa” : 8, “Bart”:10,”Marge”:35,”Homer”:40,”Santa”:2}

Write a code that combines the two dictionaries to generate the following output:
The Simpsons: 
Homer is a father and is 40 years old 
Lisa is a daughter and is 8 years old 
Bart is a son and is 10 years old
Santa is a dog and is 2 years old 
Marge is a mother and is 35 years old 

29)  Write a program that takes a list of words and creates a dictionary with word as the key  and count of word in the list as value. 

     30) Write a program that takes a list of words and creates a dictionary with count of word as 	key and list of words for that frequency as value. 

31) There are 36 possible combinations, if we throw two dice. A simple pair of loops over range (6) +1 will enumerate all combinations. The sum of the two dice is more interesting than the actual combination. Create a dictionary of all combinations, using the sum of numbers on the two dice as the key. Each value of dictionary will be a tuple with all possible combination.
32) Using turtle.py library do the following –
i) Create a function polygon (n, side, color) to create a regular polygon with given n sides and side as length of one side and color.
ii) Add another function flower (leaves, side ) which uses above polygon of specified side as defined in above function and creates a flower of given leaves where each leaf is made up of a polygon.
Create now multiple instances of turtle and display a bouquet of flowers of different color and shape.

**Project Work –**
     Prepare a project using any one additional library like graphics / tkinter /turtle to simulate any real life process/ game / simulation / tutorial. 

### Mrinal Wahal ###