#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 11                            #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os, getpass, re

class ValidPwd:
    
    def __init__(selfie): selfie.passwords = []
    
    def validate(selfie):
        
        permission = raw_input("Do You Wish The Password To Be Shown ? (Y/N): ")
        perm = permission.lower()
        
        if perm == "n": pwds = getpass.getpass("Enter Desired Passwords: ")
        else: pwds = raw_input("Enter Desired Passwords: ")
        
        for password in pwds.split(","):
            if re.search("(\w+)(\d+)($|#|@+)", password) and len(password) > 6 and len(password) < 12: selfie.passwords.append(password)
    
    def display(selfie): print "Possible Passwords: ", selfie.passwords        

def main():
    
    print """

NOTE: Entered Passwords Must Be Seperated By Commas.

Following Are The Criteria For Checking A Password:

1. At least 1 letter between [a-z]
2. At least 1 number between [0-9]
3. At least 1 letter between [A-Z]
4. At least 1 character from [$#@]
5. Minimum length of transaction password: 6
6. Maximum length of transaction password: 12

    """
    try:
        alpha = ValidPwd()
        alpha.validate()
        alpha.display()
    
    except Exception, e: print "Error: " + str(e)    
    
    finally:
        print
        os.system("pause")
    
if __name__ == '__main__': main()