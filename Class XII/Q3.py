#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 3                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class Point:
    
    def __init__(selfie):
        
        selfie.x = 0
        selfie.y = 0
        
    def locn(selfie,a,b):
        
        global location
        location = [int(a),int(b)]
    
    def dist_from_orig(selfie) : return ((location[0] - selfie.x) + (location[1] - selfie.y))**(0.5)
        
    def dist_betwixt (selfie, point2): return ((point2[0] - location[0]) + (point2[1] - location[1]))**(0.5)
   
    def __str__(selfie, point2):
        
        if point2[0] == 0 & point2[1] == 0: print "Point Is On The Origin."
        if point2[0] < 0 & point2[1] < 0: print "Point Is In The 3rd Quadrant."
        if point2[0] > 0 & point2[1] > 0: print "Point Is In The 1st Quadrant."
        if point2[0] > 0 & point2[1] < 0: print "Point Is In The 2nd Quadrant."
        else: print "Point Is In The 4th Quadrant."

def main():
    
    try:
        location1 = raw_input("Enter The Location Of Point 1 (X,Y): ")
        point_str = location1.split(",")
        
        alpha = Point()
        alpha.locn(point_str[0], point_str[1])
        
        point2 = []
        point2_str = raw_input("Enter The Location Of Point 2 (X,Y): ")
        point2_temp = point2_str.split(",")
        for element in point2_temp: point2.append(int(element))
        
        print "\nDistance Of Point 1 From The Origin Is: ", alpha.dist_from_orig()
        print "Distance Bet. Both Points: ", alpha.dist_betwixt(point2)
        alpha.__str__(point2)
    
    
    except Exception, e: print "Error: " + str(e)
    
    finally:
        print
        os.system("pause")
        
if __name__ == '__main__': main()
    