#--------------------------------------------------
# Project : Amity Practical List Class XII         
# Language: MySQL                                 
# Script  : Question 1                              
# Author  : Mrinal Wahal                           
#--------------------------------------------------

SHOW DATABASES;
DROP BookHouse;

CREATE DATABASE IF NOT EXISTS BookHouse;
USE BookHouse;

DROP TABLE Alpha;
create table IF NOT EXISTS Alpha

(Title varchar(25) not null,
No MEDIUMINT NOT NULL AUTO_INCREMENT ,
Author varchar(25) not null,
Subject tinytext not null,
Publisher varchar(25) not null,
Qty int(4) not null,
Price float(5,2) not null,
PRIMARY KEY(No));

INSERT INTO Alpha
(Title, Author, Subject, Publisher, Qty, Price)
VALUES
('Data Structure', 'Lipschute', 'DS', 'McGraw',	4, 217.00),
('DOS Guide', 'Nortron', 'OS', 'PHI', 3, 175.00),
('Turboc C++', 'Robort Lafore', 'Prog', 'Galgotia',	5, 270.00),
('Dbase Dummies', 'Palmer', 'DBMS', 'PustakM', 7, 130.00),
('Mastering Windows', 'Cowart', 'OS', 'BPB', 1, 225.00),
('Computer Studies', 'French', 'FND', 'Galgotia', 2, 75.00),
('COBOL', 'Stern', 'Prog', 'John W', 4, 1000.00),
('Guide Network', 'Freed', 'NET', 'Zpress', 3, 200.00),
('Basic for Beginners', 'Norton', 'Prog', 'BPB', 3, 40.00),
('Advanced Pascal', 'Schildt', 'Prog', 'McGraw', 4, 350.00);

SELECT 'The Table Is As Followed' as '';
SELECT * FROM Alpha;
# Answer 1
SELECT Title, Price FROM Alpha WHERE Price>100 AND Price<300; 
# Answer 2
SELECT Title, Author FROM Alpha WHERE Subject='Prog' AND Publisher='BPB';
# Answer 3
SELECT MAX(No) as 'Total Books' FROM Alpha; 
# Answer 4
SELECT Publisher, SUM(Price)/SUM(Qty) as 'Average Price For Each Publisher'
FROM Alpha GROUP BY Publisher;   
SELECT Title, Price FROM Alpha ORDER BY Price DESC; 
# Answer 5
SELECT Title, Price FROM Alpha WHERE Title LIKE 'D%' AND Price>3;  
# Answer 6
SELECT Publisher, SUM(Qty*Price) as 'Stock Value' FROM Alpha 
GROUP BY Publisher; 
# Answer 7
SELECT Title as 'Costliest Book', MAX(Price) FROM Alpha;
\q