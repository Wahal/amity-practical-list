#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 10                            #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class Library:
    
    def __init__(selfie,booklist):
        
        selfie.booklist = booklist
        selfie.issued = {}
        for book in selfie.booklist: selfie.issued[book] = (None,None)
        
    def checkout(selfie,book,member,date):
        
        global duedate
        duedate = date + 7
        selfie.issued[book] = (member,duedate)
        print  member, "Has Succesfully Checked Out The Book: ", book, ". \nDue Date Of The Book: ", duedate
        
    def checkin(selfie,book,date,member):
        
        constant = False
        for name in selfie.issued:
            if name == book and selfie.issued[name][0] == member:
                constant = True
                overdue = (date - duedate)
                if overdue > 0: print "You Were Late. You've Been Fined For Rs.", (0.25)*overdue
                selfie.issued[book] = (None,None)
                print  member, "Has Succesfully Checked In The Book: ", book
        
        if constant == False: print "No Member Found For The Given Book To Check In."
            
    def overdue_books(selfie,member,date):
        
        constant = False
        for value in selfie.issued.values():
            if value[0] == member and date - value[1] > 0:
                constant = True
                for key in selfie.issued.keys():
                    if selfie.issued[key][0] == member: print "As Of Now", member,"Has Overdue Book(s): ", key,
            
        if constant == False: print "You Have No Overdue Books."
            
def main():
    
    try:
        books = raw_input("Enter The List Of Books (Seperated By Commas): ")
        name = raw_input("Kindly Enter Member's Name: ")
        book_for_checkout = raw_input("Enter The Book You Wish To Check Out: ")
        book_for_checkin = raw_input("Enter The Book You Wish To Check In: ")
        checkout_date = input("Enter Check Out Date (Only The Day): ")
        checkin_date = input("Enter Check In Date (Only The Day): ")
        books_list = books.split(",")
        alpha = Library(books_list)
        alpha.checkout(book_for_checkout,name,checkout_date)
        alpha.checkin(book_for_checkin,checkin_date,name)
        perm_for_overdue = raw_input("Do You Wish To Check For Overdue Books ? (Y/N): ")
        
        if perm_for_overdue.lower() == "y":
            date = input("Enter Current Date (Only The Day): ")
            alpha.overdue_books(name,date)
    
    except Exception, e: print "Error: " + str(e) + "\n"
    
    finally:
        print
        os.system("pause")
    
if __name__ == '__main__': main()
    