#!/usr/bin/env python

print """
#--------------------------------------------------#
# Project : Amity Practical List Class XII         #
# Language: Python                                 #
# Script  : Question 9                             #
# Author  : Mrinal Wahal                           #
#--------------------------------------------------#
"""

import os

class Clock(object):
    
    def __init__(selfie):
        
        super(Clock,selfie).__init__()
        clock = set_clock(selfie)        
        selfie.hours = clock[0]
        selfie.minutes = clock[1]
        selfie.sec = clock[2]
        selfie.time = __str__(selfie)
        
    global set_clock
    def set_clock(selfie):
        
        hours = input("Enter Hours: ")
        minutes = input("Enter Minutes: ")
        sec = input("Enter Seconds: ")
        
        if hours > 60 and minutes > 60 and sec > 60: print "Error: Time Values Cannot Exceed 60."
        elif hours > 60: print "Error: Time Values Cannot Exceed 60."
        elif minutes > 60: print "Error: Time Values Cannot Exceed 60."
        elif sec > 60: print "Error: Time Values Cannot Exceed 60."
        else: return [hours, minutes, sec]
        
    global __str__
    def __str__(selfie): return str(selfie.hours) + " : " + str(selfie.minutes) + " : " + str(selfie.sec)
    
    def tick(selfie): selfie.sec += 1
    
class Calendar(object):
    
    def __init__(selfie):
        
        super(Calendar,selfie).__init__()
        
        calendar = set_calendar(selfie)
        
        selfie.day = calendar[0]
        selfie.month = calendar[1]
        selfie.year = calendar[2]
        selfie.date = __str__(selfie)
        leapyear(selfie)
        
    global set_calendar
    def set_calendar(selfie):
        
        day = input("Enter Day: ")
        month = input("Enter Month: ")
        year = input("Enter Year: ")
        
        if len(str(year)) < 4 or day > 31: print "Error: Year Should Be a 4 Digit Value."
        elif len(str(year)) < 4 and day > 31: print "Error: Year Should Be a 4 Digit Value."
        else: return [day,month,year]
        
    global __str__
    def __str__(selfie): return str(selfie.day) + " | " + str(selfie.month) + " | " + str(selfie.year)
    
    global leapyear
    def leapyear(selfie):
        
        if selfie.year % 400 == 0: print "It's a Leap Year."
        elif selfie.year % 400 != 0 and selfie.year % 100 == 0: print "It's Not a Leap Year."    
        elif selfie.year % 4 == 0 and selfie.year % selfie.year != 0: print "It's a Leap Year."
        else: print "It's Not a Leap Year."
        
    def advance(selfie): selfie.day += 1
    
class CalendarClock(Clock,Calendar):
    
    def __init__(selfie):
    
        super(CalendarClock,selfie).__init__()
        
        while selfie.hours < 24:
            while selfie.minutes < 60:
                while selfie.sec < 60:
                    selfie.sec += 1

                selfie.sec = 0
                selfie.minutes += 1

            selfie.minutes = 0
            selfie.hours += 1
            
        selfie.day += 1
        print "New Date: ", selfie.day, "|", selfie.month, "|", selfie.year
            
def main():
    
    try: CalendarClock()
    
    except Exception, e: print "Error: " + str(e)
    
    finally:
        print
        os.system("pause")
        
if __name__ == '__main__': main()